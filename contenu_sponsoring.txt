 remaining :
-Speakers
-Programme
-Programme de communication
-Liste des besoins.

Qui sommes-nous ?
Le club Open Minds est un club scientifique
estudiantin de l’USTHB créé en 2009, son
but premier est de faire la promotion de
l’esprit de partage entre étudiants ainsi que
de la philosophie Open Source.

Nos objectifs :
-Promouvoir et vulgariser l'esprit du Libre Open Source.
-Encourager l’entraide estudiantine et développer l’esprit de partage chez les étudiants.
-Optimiser l'exploitation du potentiel intellectuel des membres.
-Développer et entretenir des réseaux de relation interne et externe. 

Nos activités :
Les objectifs du club se voient réalisés à travers l'organisation de diverses activités,
Dont les plus importantes :
FORMATIONS & ATELIERS :
Afin de concrétiser l'esprit du partage du club, des sessions de formations et d'ateliers y sont dispensées, par des étudiants expérimentés ou des diplômés souhaitant faire profiter leur savoir dans un quelconque domaine.

LE GROUPE DE DÉBAT, THE GREAT MINDS OF USTHB :
Espace propice à l’échange d’idées, le développement de l’esprit critique et couveuse de débats fructueux. Voilà ce à quoi aspire à être TGM.

OMMAGAZINE :
Il s'agit d'une revue qui s’intéresse entre autres à l'actualité du monde, des nouvelles technologies, découvertes scientifiques et culturelles.


NAUTILUS :
Activité littéraire visant à enrichir et développer l’aspect littéraire chez les étudiants via des sessions de discussion abordant une thèmatique littéraire préalblement définie.

AWAKEN CURIOSITY :
Un workshop visant à développer le leadership, time management, communication, et prise de décision.




Nos projets :
Le club incite ses adhérents à réaliser des projets collaboratifs mettant en pratique plusieurs spécialités où les étudiants échangent et s’entraident pour le bien du projet.

YOUR WAY INTO LIBRE OPEN SOURCE
Des sessions de formations organisées en collaboration avec Mozilla Algérie. Le but de ce projet était d'encourager les étudiants à contribuer à des projets libres et open source.

JERRYSCHOOL
Le club a accueilli pendent une semaine la première édition du jerryschool en Algérie, le but est de fabriquer des ordinateurs avec des composant de récupération assemblés dans un jerrycan.
         
FOSJUMP : YOUR FIRST FREE/OPENSOURCE JUMP
Série d’ateliers permettant aux participants de se familiariser avec les outils de travail collaboratif.

E-LOCK 
Ce projet vise à sécuriser l’accès au local du club en installant une serrure électronique. 

BIBLIOTHÈQUE 
Le club dispose de sa propre bibliothèque qui s'agrandit grâce aux contributions des étudiants.




Nos évènements :
Ces événements visent à impliquer des personnes internes et externes et perpétuer ainsi l'esprit de partage.
 

GNU/LINUX INSTALL PARTY
Événement phare du club, organisé depuis sa création en 2009.

PI DAY
Co-organisé avec le Club des Activités Polyvalentes de l’École Polytechnique Alger, PiDay est un événement qui profite de la date du  03.14.15 pour célébrer les mathématiques à travers conférences, quizz, énigmes et expositions de jeux liés à la logique mathématique.

ARDUINO TA3I
Événement d'initiation à Arduino, organisé en collaboration avec Open Hardware Algérie et le club Inelectronics de l’Université de Boumerdes.

EARTHQUAKES :
Événement visant à vulgariser le domaine de la sismologie et sensibiliser les gens aux bonnes pratiques dans ces cas là.

INFORMATHICS
Événement qui vise à illustrer au travers d'une série de conférences le lien entre les mathématiques et l’informatique en citant des exemples concrets.

ARDUINO SETUP
Un bootcamp de 2 jours visant à initier les participants à l'open hardware et l'arduino au travers de conférences et workshops, pour passer à la réalisation de projets arduino concrets.





COMMUNICATION :

Notre page Facebook est un important moyen de communication qui regroupe déjà de nombreux utilisateurs, plus de 
16000 fans qui peuvent suivre nos actions en temps réel.

Notre communications se fait essentiellement via :

-Notre site web openmindsclub.net
-Des campagnes de communications visuelles : Par l’intermédiaire d’affiches, posters, t-shirts, flyers, et banners.
-Supports audiovisuels (passage radio, passage TV, articles au niveau de la presse locale).
-Notre magazine digital OMMagazine.
-L’animation de stands lors de divers événements organisés par d’autres clubs notamment le CAP, ainsi que lors de la semaine d’anniversaire de l’USTHB.

Nous sommes aussi présent sur :
Twitter... Instagram... YouTube... snapchat...


-Bureau executif 2017/2018
-Les parains (keep the same ones). 

Ils nous ont fait confiance : 
Mozilla Algeria, ENG (Entreprise nationale des granulats), Github, Open Invention Network, CAP, GDG
Institut français, Agence universitaire de la froncofonie (AUF)







I. Qu'est ce que l'install party ?
L'install party est l'événement phare du club et a pour but de faire connaître ce qu'est l'Open Source ainsi que son apport essentiel dans tous les domaines de notre quotidien et ce, à travers des talks grand public, workshops et d'autres activités. l'Install Party est déstiné à toute personne s'intéressant au domaine des sciences, de l'IT et désireuse d'y contribuer ce qui est l'essence même de l'Open Source.

II. À qui ?
L'install party est un événement national grand public, tout le monde peut y participer et parmi ce public nous comptons :
-La communauté estudiantine de l'USTHB qui représente un peu plus de 40000 étudiants.
-Étudiants des universités et écoles environnantes : Blida, Boumerdes, ESI, Polytech, EXEA.
-Clubs scientifiques de l'USTHB et autres universités.
-Représentants du monde professionnel.
-Toute personnne qui s'intéresse au domaine des sciences et IT.

III. Install party 9
Thème : Open Source Achivements (To change of course)
L'idée derrière cette thématique est de pouvoir atteindre le plus large public possible en montrant ce qu'a pu apporter l'Open Source dans chaque domaine de nos vies ainsi que l'importance de l'esprit participatif 
et ce à travers des talks et activités expliquant le fonctionnement de cette philosophie mais également parler d'outils et projets communautaires concrets qui ont révolutionné leur domaine et comment y contribuer.

Quand : 14/21 Avril 2018
Où : Cyber espace USTHB.

IV. Pourquoi devenir sponsor
Auprés du grand public l'image du sponsor sera associé à un événement de l'un des clubs les plus actifs de l'USTHB s'intéressant
à un sujet d'actualité qui est très répondu à l'étranger et qui commence à s'installer en Algerie. La visibilité du sponsor se fera
auprès d'un public large s'intéressant à la scène nationale des TIC et ses nouveautés.

C'est une oppurtunité de :
-Lier votre image à un événement estudiantin faisant la promotion de l'Open Source et de sa communauté qui ne cesse de croître.
-Une opportunité pour conforter votre  image en visibilité et accroître votre notoriété en bénéficiant d'un impact médiatique avant pendant et APRÈS l'événement car les talks et activités seront mis en ligne et seront visionnés par des milliers de gens.
-Associer votre entreprise au développement technologique et scientifique du pays et porter le titre d'inverstisseur nationale.


V. Offres de sponsoring
Gold :
-Votre logo mis en avant sur tous les supports de communication.
-Publicité sur nos réseaux sociaux comme le specifie le planning de communication ainsi que sur le site de l'événement.
-Citation de votre entreprise lors des passages radio, TV et articles de presse. (À vérifier légalement parlant)
-Mention lors de notre campagne d'emailing.
-remérciments lors de la cérémonie d'ouverture et clôture.
-Installer des supports de communication de votre entreprise.


Silver :
-Votre logo sur tous les supports de communication.
-Publicité sur nos réseaux sociaux ainsi que le site.
-Citation de votre entreprise lors des passages radio, TV et articles de presse.
-remérciments lors de la cérémonie d'ouverture et de clôture.


Liste des besoins :
Logistiques :
(materiel installations)
-30 Clés USB 4Go 30x800 
-1 Switch Gigabyte 16 ports 14000
-2 boites DVD 1750x2
(materiel audio visuel)
-Location Sono + micros 50000
(Collation)
-Pause café pour 300 personnes 300x150 
-Dejeuner pour 250 personnes (buffet) + 50 repars VIP (Sponsors et personnalités). 250x200 + 50x1300

Total Logistique : 211000 DA

Communication :
-75 Tee-shirts 75x1200
-2 banners (Contenu event + liste partenaires). 2x9500
-1 bâche (sponsors wall). 8000 
-400 Portes badge. 400 x 30 
-400 blocs notes pour participants. 400x30
-750 Flyers. 750x10
-600 Stickers (200 Open Minds Club + 200 Install party + 200 Sponsor Gold) 600x25
-Cadeaux tombola (Mugs, pack de stickers + proposition du sponsor Gold) 15000 DA 

Total communication : 165000 DA

Total liste des besoins : 376000 
(afin de maximiser les supports de communication pour mettre avant le(s) sponsors 
nous proposons également un plafond à 450000 DA).

Gold à partir de : 290000 DA
Silver (jusqu'à 2 sponsors) à partir de : 160000 DA.